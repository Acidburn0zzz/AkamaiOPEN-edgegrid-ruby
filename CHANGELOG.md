1.0.1
-----
* Change POST behavior to truncate and max_body to 128kb (GRID-236)

1.0
---
* Initial release
